﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_Parcial_1
{
    class Bicicleta
    {
        public string color = "";
        public string Marca = "";
        public string Tipo = "";
        public int Precio = 0;
        public int NumLlanta = 0;

        public void setcolor(string color)
        {
            this.color = color;
        }
        public string getcolor()
        {
            return this.color;
        }

        public void setMarca(string Marca)
        {
            this.Marca = Marca;
        }
        public string getMarca()
        {
            return this.Marca;
        }

        public void setTipo(string Tipo)
        {
            this.Tipo = Tipo;
        }
        public string getTipo()
        {
            return this.Tipo;
        }

        public void setPrecio(int Precio)
        {
            this.Precio = Precio;
        }
        public int getPrecio()
        {
            return this.Precio;
        }
        public void setNumLlanta(int NumLlanta)
        {
            this.NumLlanta = NumLlanta;
        }
        public int getNumLlanta()
        {
            return this.NumLlanta;
        }
    }
}
