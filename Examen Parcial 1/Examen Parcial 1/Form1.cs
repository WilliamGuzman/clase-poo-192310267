﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen_Parcial_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bicicleta Bici = new Bicicleta();

            Bici.setcolor("Azul");
            string ColorB = Bici.getcolor();

            Bici.setMarca("Mongoose");
            string MarcaB = Bici.getMarca();

            Bici.setTipo("Montaña");
            string TipoB = Bici.getTipo();

            Bici.setPrecio(5500);
            int PrecioB = Bici.getPrecio();

            Bici.setNumLlanta(22);
            int NumLlantaB = Bici.getNumLlanta();

           

            MessageBox.Show("El Color de la Bicicleta es: " + ColorB);
            MessageBox.Show("La Marca de la Bicicleta es: " + MarcaB);
            MessageBox.Show("El Tipo de la Bicicleta es: " + TipoB);
            MessageBox.Show("El Precio de la Bicicleta es: " + PrecioB.ToString());
            MessageBox.Show("El Numero de llanta de la Bicicleta es: " + NumLlantaB.ToString() +"pulgadas");
           
        }
    }
}
