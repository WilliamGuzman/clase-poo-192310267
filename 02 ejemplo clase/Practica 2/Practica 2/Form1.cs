﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            TV TV1 = new TV();

            TV1.setTamanio(30);
            int TamanioTV1 = TV1.getTamanio();

            TV1.setVolumen(45);
            int VolumenTV1 = TV1.getVolumen();

            TV1.setColor("azul");
            string ColorTV1 = TV1.getColor();

            TV1.setBrillo(80);
            int BrilloTV1 = TV1.getBrillo();

            TV1.setContraste(60);
            int ContrasteTV1 = TV1.getContraste();

            TV1.setMarca("Samsung");
            string MarcaTV1 = TV1.getMarca();

            MessageBox.Show("El tamaño de la TV1 es: " + TamanioTV1.ToString() + "Pulgadas");
            MessageBox.Show("El volumen de la TV1 es: " + VolumenTV1.ToString());
            MessageBox.Show("El Color de la TV1 es: " + ColorTV1);
            MessageBox.Show("El brillo de la TV1 es: " + BrilloTV1.ToString() + "%");
            MessageBox.Show("El contraste de la TV1 es: " + ContrasteTV1.ToString() + "%");
            MessageBox.Show("La marca de la TV1 es: " + MarcaTV1);
        }
    }
}
