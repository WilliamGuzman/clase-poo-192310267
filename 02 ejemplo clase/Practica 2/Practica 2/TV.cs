﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_2
{
    class TV
    {
        public int Tamanio = 0;
        public int Volumen = 0;
        public string Color = "";
        public int Brillo = 0;
        public int Contraste = 0;
        public string Marca = "";
        public void setTamanio(int Tamanio)
        {
            this.Tamanio = Tamanio;
        }
        public int getTamanio()
        {
            return this.Tamanio;
        }

        public void setVolumen(int Volumen)
        {
            this.Volumen = Volumen;
        }
        public int getVolumen()
        {
            return this.Volumen;
        }

        public void setColor(string Color)
        {
            this.Color = Color;
        }
        public string getColor()
        {
            return this.Color;
        }

        public void setBrillo(int Brillo)
        {
            this.Brillo = Brillo;
        }
        public int getBrillo()
        {
            return this.Brillo;
        }

        public void setContraste(int Contraste)
        {
            this.Contraste = Contraste;
        }
        public int getContraste()
        {
            return this.Contraste;
        }

        public void setMarca(string Marca)
        {
            this.Marca = Marca;
        }
        public string getMarca()
        {
            return this.Marca;
        }
    }
}
