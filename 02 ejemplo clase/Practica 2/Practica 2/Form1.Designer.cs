﻿
namespace Practica_2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnClase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnClase
            // 
            this.btnClase.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnClase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClase.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClase.Font = new System.Drawing.Font("Myanmar Text", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClase.Location = new System.Drawing.Point(3, 12);
            this.btnClase.Name = "btnClase";
            this.btnClase.Size = new System.Drawing.Size(125, 26);
            this.btnClase.TabIndex = 0;
            this.btnClase.Text = "Propiedades de TV";
            this.btnClase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClase.UseVisualStyleBackColor = false;
            this.btnClase.Click += new System.EventHandler(this.btnClase_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(329, 233);
            this.Controls.Add(this.btnClase);
            this.Name = "Form1";
            this.Text = "form 1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClase;
    }
}

